---
marp: true
theme: uncover
_class: lead
paginate: false
backgroundColor: #2B2F33
color: white
_footer: ""
footer: An introduction to git | 5 feb 2022 | © Sakiut
style: |
  section {
    font-family: Open Sans
  }

  section h1,h2,h4,a,a:hover {
    color: #00AEE8;
  }
  
  section strong {
    color: #F75406;
  }

  section p { font-size: 20pt }
  section li {
    list-style: none;
    font-size: 28pt;
  }
  section li::before {
    content: "\00BB\0020"
  }

  section h6 { 
    font-size: 22pt;
    margin-bottom: unset;
  }

  ::after {
    color: #fff !important;
    background: linear-gradient(-45deg,rgba(0, 0, 0, 0.2) 50%,transparent 50%) !important;
  }
---

# An introduction to git

### Sakiut

##### 5 feb 2022

---

## Contents

 - Git as a **local versioning** system
 - What about **remote**?
 - The git **ecosystem**
 - Git in a production-level **development process**

---

# Git as a local versioning system

---

<!-- header: _Git as a local versioning system_ -->

![bg left](https://i.imgur.com/lyQBLuF.png)

First of all : do **not** use command-line until you really know what you're doing !

There are lots of tools much easier to get started with:

<br>

[*VSCode's Git Graph plugin*](https://marketplace.visualstudio.com/items?itemName=mhutchie.git-graph)

[*JetBrains VCS Control*](https://www.jetbrains.com/help/idea/version-control-tool-window.html)

[*Sublime Merge*](https://www.sublimemerge.com/)

---

#### How does a Version Control System works?

Git stores the differences *line by line* between two "checkpoints", two **commits**.

![](https://i.imgur.com/eIrL5ut.png)

---

#### How does a Version Control System works?

By **applying** or **reverting** these changes, git makes you able to **move** from one commit to another. This process is called **checking out** a commit.

<br>

![w:128](https://i.imgur.com/EnwG9NQ.png)

---

#### Basic rules

 - Commit **as often as possible** (at least once a day)
 - Do not checkout when you have unapplied changes

--- 

#### Summary

**Commit**: Creates a "checkpoint" that you will be able to checkout on later
**Checkout**: Moves your current context from one commit to another
**Stash**: Shelves temporarly your unapplied changes in a stack

:warning: *To be used carefully*
**Reset**: Resets your context and your git history to an older commit 

---

#### Branches

The branches enables you to **isolate** some commits from the main thread context.

Really useful for **testing** a feature without impacting the main branch, or to develop a specific feature.

Once you've finished, you can integrate your branch modifications in the main branch, this operation is called a **merge**.

<br>

![w:350](https://raw.githubusercontent.com/gist/Sakiut/77ed682ee4572defc19f4b4fa5d23048/raw/ed53600ccde196e6fc7e7e572fd6cfd447c35452/branches.svg)

---

<!-- _header: "" -->

## What about remote?

---

<!-- header: _What about remote?_ -->

#### Remote repository

Git offers the possibility to store a server copy of your reposity. This is called a **remote**.

Unlike some others VCS like svn, git works as a **decentralized** system : you work with a **fully-functionnal local instance** of the repository that you often synchronize with the remote. This makes you able to work without access to the server.

---

#### Summary

**Fetch**: Retrieve the remote repo status without any changes locally.
**Pull**: Gets the latest version of the current branch.
**Push**: Update the remote based on your local changes.

###### :warning: *To be used carefully*
**Force Push**: Same than push but with the ability to *rewrite history*. (dangerous)

###### :bulb: *Hint*
The default remote name is usually **origin**.

---

<!-- _header: "" -->

## The git ecosystem

---

<!-- header: _The git ecosystem_ -->

#### CI/CD

Continuous **Integration** / **Deployment** / **Delivery** (AKA **Pipelines**)

Automated process which launches **at each push** on the remote repository.

###### _Usages_
Launch your unit or functional **tests**
**Build** your container or your package
**Deploy** your solution in the environment of your choice

---

![bg right:45%](https://i.imgur.com/rzyMiRe.png)

#### Project Management

Some remote hosts softwares such as **GitHub** or **GitLab** offers built-in Project Management tools designed to work with git.

---

<!-- _header: "" -->

## Git in a production-level development process

---

<!-- header: "Git in a production-level development process" -->

#### Process description

Every feature is made on a **specific branch**.

When the feature is fully developed and tested locally, it is merged to an **integration** branch where it is tested again.

Then, the integration branch's changes are merged to the main branch which will deploy in production through CI/CD.

**Direct changes to the main branch are prohibited** except when a hotfix is urgently needed.

---

#### Result in git history

![bg right:45%](https://i.imgur.com/RJG1W8z.png)

---

<!-- _header: "" -->

## Thank you for your attention!
